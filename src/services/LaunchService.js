import spaceXService from './spaceXService';

const serviceUrl = `/launches`;

class LaunchService {
  get() {
    return spaceXService.get(serviceUrl);
  }
}

const launchService = new LaunchService();

export default launchService;
