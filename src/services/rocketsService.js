import spaceXService from "./spaceXService";

const serviceUrl = `rockets/`;

class RocketsService {
    getAll() {
        return spaceXService.get(serviceUrl);
    }
}

const rocketsService = new RocketsService();
export default rocketsService;
