import React from 'react';
import PropTypes from 'prop-types';
import RocketsContext from '../stores/RocketsContext';

export const launchDetailsShape = PropTypes.shape({
  mission_name: PropTypes.string.isRequired,
  flight_number: PropTypes.number.isRequired,
  rocket: PropTypes.shape({
    rocket_id: PropTypes.string.isRequired,
  }).isRequired,
});

const KEY_ENTER = 'Enter';

const RocketDetailsTemplate = (rocketData) => {
  return <div>
    <div>Rocket Id: {rocketData.rocket_id}</div>
    <div>Cost per launch: {rocketData.cost_per_launch}</div>
    <div>Description: {rocketData.description}</div>
  </div>;
};

const LaunchTemplate = (params) => {
  const { isOpen, launchIndex, handleLaunchClick, handleKeyPress, launchDetails } = params;

  return (
    <li>
      <details open={isOpen}>
        <summary
          role="button"
          aria-expanded={isOpen ? 'true' : 'false'}
          tabIndex={launchIndex}
          onClick={handleLaunchClick}
          onKeyPress={handleKeyPress}
        >
          {launchDetails.mission_name} (Flight Number: {launchDetails.flight_number})
        </summary>

        <RocketsContext.Consumer>
          {(context) => {
            const rocketData = context.getRocketById(launchDetails.rocket.rocket_id);
            return RocketDetailsTemplate(rocketData);
          }}
        </RocketsContext.Consumer>
      </details>
    </li>
  );
}

const Launch = (props) => {
  const { launchIndex, onSelectLaunch } = props;

  const handleLaunchClick = (event) => {
    event.preventDefault();

    onSelectLaunch(launchIndex);
  };

  const handleKeyPress = (event) => {
    event.preventDefault();

    if (event.key !== KEY_ENTER) {
      return;
    }

    onSelectLaunch(launchIndex);
  };

  return LaunchTemplate({ ...props, handleLaunchClick, handleKeyPress });
}

Launch.propTypes = {
  launchDetails: launchDetailsShape.isRequired,
  launchIndex: PropTypes.number.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
};


export default Launch;
