import React from 'react';

const RocketsContext = React.createContext({
  setRockets(rockets) {
    this.rockets.splice(0);
    this.rockets.push(...rockets);
  },
  rockets: [],

  getRocketById(rocketId) {
    return this.rockets.find(rocket => rocket.rocket_id === rocketId);
  },
});

export default RocketsContext;
