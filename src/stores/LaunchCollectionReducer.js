import { ACTIONS } from '../actions/Launches';

const initialState = {
  launchesRequested: false,
  launches: [],
  fetching: false,
};

const actionHandlers = {
  [ACTIONS.REQUEST_LAUNCHES]: ({ state }) => ({
    ...state,
    launchesRequested: true,
    fetching: true,
  }),
  [ACTIONS.RECEIVE_LAUNCHES]: ({ state, action }) => ({
    ...state,
    fetching: false,
    launches: action.payload.launches,
  }),
};

export default (state = initialState, action) =>
  actionHandlers[action.type] ? actionHandlers[action.type]({ state, action }) : state;
