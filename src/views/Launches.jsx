import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import ConnectedView from './ConnectedView';
import { fetchLaunchesIfNeeded } from "../actions/Launches";
import Launch, { launchDetailsShape } from '../components/Launch';
import rocketsService from '../services/rocketsService';
import RocketsContext from '../stores/RocketsContext';

const ALL_UNSELECTED_VALUE = -1;

/**
 * Loading Template
 */
const LoadingTemplate = () => <div> LOADING </div>;

/**
 * No Launches Template
 */
const NoLaunchesTemplate = () => <div> NO DATA </div>;

/**
 * Launch List Template
 */
const LaunchListTemplate = (params) => {
  const { launchCollection, openedFlightIndex, onSelectLaunch } = params;

  const launches = launchCollection.launches.map((launchDetails, index) => {
    const isOpen = index === openedFlightIndex;

    return <li>
      <Launch {
        ...{
          key: index,
          launchDetails,
          launchIndex: index,
          isOpen,
          onSelectLaunch,
        }
      } />
    </li>
  });

  return <ul className="launchList">{launches}</ul>;
}

/**
 * Launches Inner Template Delegation
 */
const LaunchesInnerTemplate = (params) => {
  const { launchCollection } = params;
  if (!launchCollection || launchCollection.fetching) {
    return LoadingTemplate();
  }

  if (!launchCollection.launches.length) {
    return NoLaunchesTemplate();
  }

  return LaunchListTemplate(params);
}

/**
 * Launches Component Template
 */
const LaunchesTemplate = (params) => {
  return (
    <div>
      <h2> SpaceX launches </h2>
      {LaunchesInnerTemplate(params)}
    </div>
  );
};

/**
 * Launches Component
 */
const LaunchesView = (props) => {
  const { dispatch, launchCollection } = props;

  const [openedFlightIndex, setOpenedFlightIndex] = useState(ALL_UNSELECTED_VALUE);

  // TODO: not sure if an effect does anything for us here
  useEffect(() => {
    fetchLaunchesIfNeeded({ dispatch, launchCollection });
  }, [dispatch, launchCollection]);

  const rockets = useContext(RocketsContext);

  useEffect(() => {
    rocketsService.getAll().then(response => rockets.setRockets(response.data));
  }, [rockets]);


  const onSelectLaunch = (newOpenedIndex) => {
    if (openedFlightIndex === newOpenedIndex) {
      setOpenedFlightIndex(ALL_UNSELECTED_VALUE);
    } else {
      setOpenedFlightIndex(newOpenedIndex);
    }
  }

  return LaunchesTemplate({ launchCollection, openedFlightIndex, setOpenedFlightIndex, onSelectLaunch });
};

LaunchesView.propTypes = {
  dispatch: PropTypes.func.isRequired,
  launchCollection: PropTypes.shape({
    launchesRequested: PropTypes.bool.isRequired,
    launches: PropTypes.arrayOf(launchDetailsShape).isRequired,
    fetching: PropTypes.bool.isRequired,
  }).isRequired,
}

export default ConnectedView(LaunchesView, 'launches');
